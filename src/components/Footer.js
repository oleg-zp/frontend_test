import React from 'react';
import style from './Footer.module.scss';
import Store from '../store';


const Footer = () => {
  const [state] = React.useContext(Store);
  const { active } = state;

  return (
    <div className={style.content}>{active.title}</div>
  )
};

export default Footer