import React from 'react';
import Store from '../store';
import style from './Header.module.scss';
import { toggleSidebar } from '../store/actions';

const Header = () => {
  const [state, dispatch] = React.useContext(Store);
  const { active } = state;
  const openSidebar = () => toggleSidebar(dispatch);

  return (
    <div className={style.content}>
      <div onClick={openSidebar} className={style.avatar}/>

      <div>
        <span onClick={openSidebar}>{ active.title }</span>
      </div>
    </div>
  )
};

export default Header;