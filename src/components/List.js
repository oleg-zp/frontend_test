import React from 'react';
import style from './List.module.scss';
import Store from '../store';

const List = () => {
  const [state] = React.useContext(Store);
  const { active } = state;

  return (
    <div className={style.content}>
      {
        (active.members || []).map(member => (
          <div className={style.member} key={member.name}>
            <div className={style.member_name}> { member.name } </div>
            <div className={style.member_desc}> { member.desc } </div>
          </div>
        ))
      }
    </div>
  )
};

export default List;