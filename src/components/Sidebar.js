import React from 'react';
import style from './Sidebar.module.scss';
import Store from '../store';
import { setActive, toggleSidebar } from '../store/actions';
import {CSSTransition} from 'react-transition-group';

const Sidebar = () => {
  const [state, dispatch] = React.useContext(Store);
  const { active, teams, isOpenSidebar } = state;
  const chooseTeam = (team) => {
    setActive(dispatch, team);
    toggleSidebar(dispatch);
  };

  return (
      <div className={isOpenSidebar ? style.content : style.closed}>
        <CSSTransition in={isOpenSidebar} timeout={300} classNames='sidebar' unmountOnExit>
          <div className={style.list}>
            <div>Choose your side</div>

            {
              (teams || []).map(team => (
                <div
                  key={team.id}
                  onClick={chooseTeam.bind(null, team)}
                  className={`${style.item} ${team.id === active.id ? style.active : ''}`}
                >
                  <div className={style.avatar}/>
                  <div className={style.details}> {team.title} </div>
                </div>
              ))
            }
          </div>
        </CSSTransition>

        { isOpenSidebar && <div className={style.backdrop} onClick={() => toggleSidebar(dispatch)} /> }
      </div>
  )
};

export default Sidebar;