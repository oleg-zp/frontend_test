
export const setActive = (dispatch, active) => dispatch({ type: 'SET_ACTIVE', active});

export const toggleSidebar = dispatch => dispatch({ type: 'TOGGLE_SIDEBAR' });