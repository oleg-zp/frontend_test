import React from 'react';
import reducer from './reducers';
import data from '../data';

const Store = React.createContext(void 0);
const { Provider } = Store;

export const AsReduxProvider = ({ children }) => {
  const [state, dispatch] = React.useReducer(reducer, data);
  return(
    <Provider value={[state, dispatch]}>
      { children }
    </Provider>
  )
};

export default Store