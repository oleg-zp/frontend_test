export default (state, action) => {
  switch (action.type) {
    case 'TOGGLE_SIDEBAR':
      return { ...state, isOpenSidebar: !state.isOpenSidebar };
    case 'SET_ACTIVE':
      return { ...state, active: action.active };
    default:
      return state;
  }
}