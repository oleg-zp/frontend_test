import React from 'react';
import style from './App.module.scss';
import Header from './components/Header';
import Footer from './components/Footer';
import Sidebar from './components/Sidebar';
import List from './components/List';

const App = () => (
  <div className={style.content}>
    <Header/>
    <List/>
    <Footer/>

    <Sidebar/>
  </div>
);

export default App;
