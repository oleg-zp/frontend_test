export default {
  active: {
    id: 1,
    title: 'Jedi Council',
    members: [
      {
        name: 'Obi-Wan Kenobi',
        desc: 'A legendary Jedi Master',
      },
      {
        name: 'Shaak Ti',
        desc: 'A wise and patient Jedi Master'
      }
    ]
  },

  isOpenSidebar: false,

  teams: [
    {
      id: 1,
      title: 'Jedi Council',
      members: [
        {
          name: 'Obi-Wan Kenobi',
          desc: 'A legendary Jedi Master',
        },
        {
          name: 'Shaak Ti',
          desc: 'A wise and patient Jedi Master'
        }
      ]
    },
    {
      id: 2,
      title: 'Sith Lords',
      members: [
        {
          name: 'Dart Vaider',
          desc: 'Luk father',
        },
        {
          name: 'Darth_Atrius',
          desc: 'One more lord'
        }
      ]
    },

  ]
};